function loadPage(id) {
    /* $.get( id+".html", function( data ) {
       $( "#popupPage" ).html(data);
       $( "#popupPage" ).show();
      // toggleNav();
    }); */

    $("#loading").show();
    $.ajax({
        url: ServerUrl + "readFile?file=" + id ,
        type: "GET",
        data: {},
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        traditional: true,
        success: function(data) {
            $("#popupPage").html('<style>.animatedSlow {-webkit-animation-duration:0.8s;animation-duration:0.8s;-webkit-animation-fill-mode:both;animation-fill-mode:both}.close-btn{ width: 30px;height: 30px;border-radius: 50px;border: 1px solid #fff;background-color: rgba(255,255,255,0.2);position: absolute;right: 15px;top: 20px;z-index: 999; line-height:30px; text-align:center; color:#fff !important; font-weight:bold;}.close-btn:hover{ color:#000;  background-color: rgba(255,255,255,0.5); }.cross1{ width:50%; height:3px; background-color:#fff; position:relative; top:46%; left:26%; transform: rotate(45deg);}.cross2{ width:50%; height:3px; background-color:#fff; position:relative; top:36%; left:26%; transform: rotate(-45deg);}@-webkit-keyframes DDDown {0% {opacity:0;-webkit-transform:translateY(-20px);transform:translateY(-20px)}100% {opacity:1;-webkit-transform:translateY(0);transform:translateY(0)}}@keyframes DDDown {0% {opacity:0;-webkit-transform:translateY(-20px);-ms-transform:translateY(-20px);transform:translateY(-20px) }100% {opacity:1;-webkit-transform:translateY(0);-ms-transform:translateY(0);transform:translateY(0)}}.DDDown {-webkit-animation-name:DDDown;animation-name:DDDown}ol{ padding:0 0 0 5px !important; color:#999;}li{ padding:0 0 0 8px !important; color:#999;}.termscontent{ color:#bbb;} .popUpCont h1, .popUpCont h2{ margin-top:10px !important; color:#fff;}.popUpCont p { color:#bbb;}.popUpCont a{ color:#ff3333; text-decoration:none;}.popUpCont{ width:100%; height:100vh; position:relative; top:0px; overflow-y:scroll; margin:0 auto; padding:10px;box-sizing: border-box; background-color: #000;}</style><div  class="popUpCont"><a class="close-btn" href="javascript:closePage()">X</a>'+data.content.body+'</div>');
            $("#loading").hide();
            $(".mainwrapper").hide();
            $("#popupPage").show();
            if ($("nav").length) {
                //toggleNav();
            };
        },
        error: function(jqXHR, textStatus, errorThrown) {
        },
        complete: function() {

        }
    });

}
function loadLocalPage(id) {
     $.get( id, function( data ) {      
            $("#popupPage").html('<style>.animatedSlow {-webkit-animation-duration:0.8s;animation-duration:0.8s;-webkit-animation-fill-mode:both;animation-fill-mode:both}.close-btn{ width: 30px;height: 30px;border-radius: 50px;border: 1px solid #fff;background-color: rgba(255,255,255,0.2);position: absolute;right: 15px;top: 20px;z-index: 999; line-height:30px; text-align:center; color:#fff !important; font-weight:bold;}.close-btn:hover{ color:#000;  background-color: rgba(255,255,255,0.5); }.cross1{ width:50%; height:3px; background-color:#fff; position:relative; top:46%; left:26%; transform: rotate(45deg);}.cross2{ width:50%; height:3px; background-color:#fff; position:relative; top:36%; left:26%; transform: rotate(-45deg);}@-webkit-keyframes DDDown {0% {opacity:0;-webkit-transform:translateY(-20px);transform:translateY(-20px)}100% {opacity:1;-webkit-transform:translateY(0);transform:translateY(0)}}@keyframes DDDown {0% {opacity:0;-webkit-transform:translateY(-20px);-ms-transform:translateY(-20px);transform:translateY(-20px) }100% {opacity:1;-webkit-transform:translateY(0);-ms-transform:translateY(0);transform:translateY(0)}}.DDDown {-webkit-animation-name:DDDown;animation-name:DDDown}ol{ padding:0 0 0 5px !important; color:#999;}li{ padding:0 0 0 8px !important; color:#999;}.termscontent{ color:#bbb;} .popUpCont h1, .popUpCont h2{ margin-top:10px !important; color:#fff;}.popUpCont p { color:#bbb;}.popUpCont a{ color:#ff3333; text-decoration:none;}.popUpCont{ width:100%; height:100vh; position:relative; top:0px; overflow-y:scroll; margin:0 auto; padding:10px;box-sizing: border-box; background-color: #000; }</style><div id="popUpCont" class="popUpCont"><a class="close-btn" href="javascript:closePage()">X</a>'+data+'</div>');
            $("#loading").hide();
            $(".mainwrapper").hide();
            $("#popupPage").show();
             if (($("nav").length )&& id=='myAccount.html' ){
                toggleNav();
                getuserdetails();
                getdevicepaired();
            } 
            else if(id=='search.html')
            {
                $("#popUpCont").addClass("srch_sc")
                $("#search").focus();
            }
      // toggleNav();
    });  

    $("#loading").show();
    

}

function closePage() {

    $("#popupPage").hide();
    $("#popupPage").html('');
     $(".mainwrapper").show();
    /*    if ($("nav").length) {
                toggleNav();
            };*/

}
